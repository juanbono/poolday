-module(poolday_worker).

-behaviour(gen_server).

%% API
-export([start_link/0]).

%% Callbacks
-export([init/1, handle_call/3, handle_cast/2]).

%% Public functions
start_link() -> 
  gen_server:start_link(?MODULE, [], []).

%% Callback functions
init([]) ->
  {ok, []}.

handle_call(_Msg, _From, State) ->
  {reply, State, State}.

handle_cast(_Msg, State) ->
  {noreply, State}.
