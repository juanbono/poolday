%%%-------------------------------------------------------------------
%% @doc pool top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(poolday_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

init([]) ->
  RestartStrategy = #{strategy => one_for_one},
  WorkerSup = 
    #{ id => poolday_worker_sup, 
       start => {poolday_worker_sup, start_link, []}, 
       type => supervisor
    },
  ServerSpec =
    #{id => poolday_server, start => {poolday_server, start_link, []}},
  ChildSpecs = [WorkerSup, ServerSpec],
  {ok, {RestartStrategy, ChildSpecs}}.

%%====================================================================
%% Internal functions
%%====================================================================
