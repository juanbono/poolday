-module(poolday_worker_sup).

-behaviour(supervisor).

%% API
-export([start_link/0, run_job/0, current_workers/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).
-define(WORKER_SPEC, #{id => pool_worker, start => {poolday_worker, start_link, []}}).
%%====================================================================
%% API functions
%%====================================================================

start_link() ->
  supervisor:start_link({local, ?SERVER}, ?MODULE, []).

run_job() ->
  {ok, Pid} = supervisor:start_child(?MODULE, []),
  poolday_server:notify_new(),
  {ok, Pid}.

current_workers() ->
  CountChildren = supervisor:count_children(poolday_worker_sup),
  proplists:get_value(workers, CountChildren).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

init([]) ->
  RestartStrategy = #{strategy => simple_one_for_one},
  ChildSpecs = [?WORKER_SPEC],
  {ok, {RestartStrategy, ChildSpecs}}.

%%====================================================================
%% Internal functions
%%====================================================================
