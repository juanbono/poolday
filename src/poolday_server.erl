-module(poolday_server).

-behaviour(gen_server).

%% API
-export([start_link/0, notify_new/0, run/0]).

%% Callbacks
-export([init/1, handle_call/3, handle_cast/2]).

-define(INITIAL_STATE, []).

%% Public functions
start_link() -> 
  gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

notify_new() ->
  gen_server:cast(?MODULE, worker_created).

run() -> 
  gen_server:call(?MODULE, create_worker).

%% Callback functions
init([]) ->
  {ok, ?INITIAL_STATE}.

handle_call(create_worker, _From, State) ->
  poolday_worker_sup:run_job(),
  {reply, State, State};
handle_call(_Msg, _From, State) ->
  {reply, State, State}.

handle_cast(worker_created, State) ->
  io:format("Pool Size: ~p~n", [poolday_worker_sup:current_workers()]),
  {noreply, State};
handle_cast(_Msg, State) ->
  {noreply, State}.

